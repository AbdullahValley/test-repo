<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(StudentsTableSeeder::class);
//        echo "Students Table seeded";

        $this->call(CategoriesTableSeeder::class);
        echo "Categories Table seeded";
    }
}
