@extends('admin.layouts.master')
@section('title-tag','All Deleted Students')
@section('box-title')
  Trash Records
@endsection
@section('content')
  <!-- /.box-header -->
  <div class="box-body">
    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
            <thead>
            <tr role="row">
              <th>SL</th>
              <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 230.933px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Name</th>
              <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 282.983px;" aria-label="Browser: activate to sort column ascending">Email</th>
              <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 250.983px;" aria-label="Platform(s): activate to sort column ascending">Mobile</th>
              <th class="sorting text-center" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 143.65px;" aria-label="CSS grade: activate to sort column ascending">Action</th>
            </tr>
            </thead>
            <tbody>

            <tr role="row" class="odd">
              <td class="sorting_1"> 01 </td>
              <td class="sorting_1"> John Doe </td>
              <td> john@gmail.com </td>
              <td> 01715 000 000 </td>
              <td>
                <table border="0">
                  <tr>
                    <td style="padding:0 15px;" class="text-center">
                      <a href="{{url('/admin/student/show/1')}}" class="btn btn-primary">View</a>
                    </td>
                    <td style="padding:0 15px;" class="text-center">
                      <a href="{{url('/admin/student/restore')}}" class="btn btn-primary">Restore</a>
                    </td>
                    <td style="padding:0 15px;" class="text-center">
                      <a href="{{url('/admin/student/delete')}}" class="btn btn-primary">Delete</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
@endsection
@push('script')
  <script src="{{ asset('back-end/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('back-end/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endpush
@push('script-inline')
  <script>
      $(function () {
          $('#example1').DataTable()
          $('#example2').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
          })
      })
  </script>
@endpush
