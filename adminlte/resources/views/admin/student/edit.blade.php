@extends('admin.layouts.master')

@section('title-tag','Update Student Info')

@section('box-title')
Update Student Info
@endsection

@section('content')

<!-- /.box-header -->
<div class="box-body">


    <div class="col-md-6 col-md-offset-3">
        <!-- general form elements -->
        <div class="box box-primary">


            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('admin/student') }}" method="POST">

                {{ csrf_field() }}

                <div class="box-body">

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control"  name="name" id="name" placeholder="Enter Your Name" type="text">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" name="email" id="email" placeholder="Enter Your Email" type="email">
                    </div>
                    <div class="form-group">
                        <label for="password">Mobile</label>
                        <input class="form-control" name="mobile" id="mobile" placeholder="Enter mobile Number" type="tel">
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
        <!-- /.box -->

    </div>


</div>

@endsection

