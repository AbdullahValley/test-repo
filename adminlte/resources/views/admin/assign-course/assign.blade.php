@extends('admin.layouts.master')
@push('css')

	<!-- form CSS START -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="{{ asset('back-end/plugins/iCheck/all.css') }}">
	<!-- Bootstrap Color Picker -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="{{ asset('back-end/plugins/timepicker/bootstrap-timepicker.min.css') }}">
	<!-- Select2 -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/select2/dist/css/select2.min.css') }}">
	<!-- form CSS END -->

@endpush



@section('title-tag','Assign Course')

@section('box-title')
Assign Course
@endsection



@section('content')
<div class="box-body">





      <div class="col-md-6 col-md-offset-3">
          <!-- general form elements -->
          <div class="box box-primary">


            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('admin/assign-course') }}" method="POST">

              {{ csrf_field() }}

              <div class="box-body">

                  <div class="form-group">

                      <label>Select Student : </label>


                      <select name="student_id" class="btn btn-default" required>

                          <option>Select Student Name</option>


                          @foreach($students as $student)


                          <option value="{{ $student->student_id }}">

                              {{ $student->student_name }}

                          </option>

                          @endforeach

                      </select>

                  </div>

                  <div class="form-group">

                      <label>Check Courses : </label>

                      <br><br>

                      @foreach($courses as $course)


<input id="{{ $course->course_id }}" type="checkbox" name="course_id[]" value="{{ $course->course_id }}">

<label for="{{ $course->course_id }}">{{ $course->course_name."  ` ` `  " }}</label>


                      @endforeach

                  </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
<button type="submit" class="btn btn-primary">Submit</button>
</div>

</form>
</div>
<!-- /.box -->

</div>

<!-- Basic Editor -->





<!-- Advanced Editor -->


</div>
<!-- /.col -->
@endsection



@push('script')

<!-- Add Script -->
<!-- Select2 -->
<script src="{{ asset('back-end/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('back-end/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('back-end/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('back-end/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap color picker -->
<script src="{{ asset('back-end/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ asset('back-end/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('back-end/plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<!-- Add Script END -->

@endpush




@push('script-inline')
<script>
$(function () {
//Initialize Select2 Elements
$('.select2').select2()

//Datemask dd/mm/yyyy
$('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
//Datemask2 mm/dd/yyyy
$('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
//Money Euro
$('[data-mask]').inputmask()

//Date range picker
$('#reservation').daterangepicker()
//Date range picker with time picker
$('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
//Date range as a button
$('#daterange-btn').daterangepicker(
{
ranges   : {
'Today'       : [moment(), moment()],
'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
'Last 30 Days': [moment().subtract(29, 'days'), moment()],
'This Month'  : [moment().startOf('month'), moment().endOf('month')],
'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
},
startDate: moment().subtract(29, 'days'),
endDate  : moment()
},
function (start, end) {
$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
}
)

//Date picker
$('#datepicker').datepicker({
autoclose: true
})

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
checkboxClass: 'icheckbox_minimal-blue',
radioClass   : 'iradio_minimal-blue'
})
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
checkboxClass: 'icheckbox_minimal-red',
radioClass   : 'iradio_minimal-red'
})
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
checkboxClass: 'icheckbox_flat-green',
radioClass   : 'iradio_flat-green'
})

//Colorpicker
$('.my-colorpicker1').colorpicker()
//color picker with addon
$('.my-colorpicker2').colorpicker()

//Timepicker
$('.timepicker').timepicker({
showInputs: false
})
})
</script>
@endpush

