@extends('admin.layouts.master')

@section('title-tag','Update Assigned Course')

@section('box-title')
Update Assigned Courses
@endsection

@section('content')

<!-- /.box-header -->
<div class="box-body">


    <div class="col-md-6 col-md-offset-3">
        <!-- general form elements -->
        <div class="box box-primary">


            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('admin/assign-course') }}" method="POST">

                {{ csrf_field() }}

                <div class="box-body">

                    <div class="form-group">
                        <label>Select Student</label>
                        <select class="form-control">
                            <option>Student Name 1</option>
                            <option>Student Name 2</option>
                            <option>Student Name 3</option>
                            <option>Student Name 4</option>
                            <option>Student Name 5</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Assign Course</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Laravel
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                WordPress
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Java
                            </label>
                        </div>
                    </div>



                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
        <!-- /.box -->

    </div>


</div>

@endsection

