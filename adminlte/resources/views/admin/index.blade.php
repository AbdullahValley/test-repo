@extends('admin.layouts.master')

@section('title-tag', 'Dashboard')

@section('box-title')
Dashboard
@endsection

@section('content')
<div class="box-body">
<div class="col-md-6 col-md-offset-3 text-center">
	<h1>Welcome to Dashboard</h1>

	<br>

	<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat error delectus, atque eveniet. Quidem fugiat, nobis hic nemo laudantium.</p>

	<br>
</div>
</div>
@endsection