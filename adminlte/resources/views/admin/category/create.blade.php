@extends('admin.layouts.master')
@push('css')

	<!-- form CSS START -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="{{ asset('back-end/plugins/iCheck/all.css') }}">
	<!-- Bootstrap Color Picker -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="{{ asset('back-end/plugins/timepicker/bootstrap-timepicker.min.css') }}">
	<!-- Select2 -->
	<link rel="stylesheet" href="{{ asset('back-end/bower_components/select2/dist/css/select2.min.css') }}">
	<!-- form CSS END -->

@endpush



@section('title-tag','Add Category')

@section('box-title')
Add New
@endsection



@section('content')
<div class="box-body">

<div class="col-md-6 col-md-offset-3">
<h1>Simple Form:</h1><br>
</div>




<div class="col-md-6 col-md-offset-3">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input class="form-control"  id="name"placeholder="Enter Your Name" type="text">
                </div>
                <div class="form-group">
                  <label for="username">User Name</label>
                  <input class="form-control"  id="username"placeholder="Enter User Name" type="text">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input class="form-control" id="email" placeholder="Enter Your Email" type="email">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input class="form-control" id="password" placeholder="Password" type="password">
                </div>

                <div class="form-group">
                  <label for="address">Address</label>
                  <input class="form-control"  id="address"placeholder="Enter Your Address" type="text">
                </div>

                <div class="form-group">
                  <label for="picture">Upload Image</label>
                  <input id="picture" name="picture" type="file">

                  <p class="help-block">Maximum file size 1MB.</p>
                </div>

                <div class="form-group">
                  <label>Select Option</label>
                  <select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>


                <div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      Checkbox 1
                    </label>
                  </div>

                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      Checkbox 2
                    </label>
                  </div>
                </div>


                <div class="form-group">
                  <div class="radio">
                    <label>
                      <input name="optionsRadios" id="optionsRadios1" value="option1" checked="" type="radio">
                      Radio button 1
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input name="optionsRadios" id="optionsRadios2" value="option2" type="radio">
                      Radio button 2
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input name="optionsRadios" id="optionsRadios3" value="option3" disabled="" type="radio">
                      Radio button 3
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Textarea</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

                

        </div>

<!-- Basic Editor -->

<div class="col-md-6 col-md-offset-3">
<br><br><br><h1>Advanced Form:</h1><br>
</div>






<div class="col-md-6 col-md-offset-3">
  
  <div class="form-group">
    <label>Minimal</label>
    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
      <option selected="selected">Alabama</option>
      <option>Alaska</option>
      <option>California</option>
      <option>Delaware</option>
      <option>Tennessee</option>
      <option>Texas</option>
      <option>Washington</option>
    </select> 
        <!-- / Select Option -->
  </div>
  <!-- /.form-group -->


  <div class="form-group">
    <label>Date masks:</label>
    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      <input class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" type="text">
    </div>
    <!-- /.input group -->
  </div>
  <!-- /.form-group -->


<div class="form-group">
	<label>Date range:</label>

	<div class="input-group">
	  <div class="input-group-addon">
	    <i class="fa fa-calendar"></i>
	  </div>
	  <input class="form-control pull-right" id="reservation" type="text">
	</div>
	<!-- /.input group -->
</div>
  <!-- /.form-group -->




<div class="form-group">
    <label class="">
      <input class="minimal" checked="" style="position: absolute; opacity: 0;" type="checkbox">
    </label>
    <label class="">
      <input class="minimal" style="position: absolute; opacity: 0;" type="checkbox">
    </label>
    <label>
      <input class="minimal" style="position: absolute; opacity: 0;" type="checkbox">
      This is Minimal skin checkbox
    </label>
    <!-- Check Box -->
</div>
<!-- /.form-group -->



<div class="form-group">
	<label class="">
	  <input name="r1" class="minimal" checked="" style="position: absolute; opacity: 0;" type="radio">
	</label>
	<label class="">
	  <input name="r1" class="minimal" style="position: absolute; opacity: 0;" type="radio">
	</label>
	<label>
	  <input name="r1" class="minimal"style="position: absolute; opacity: 0;" type="radio">
	  This is Minimal skin radio
	</label>
	<!-- radio Button -->
</div>
<!-- /.form-group -->


<div class="form-group">
    <label>Color picker:</label>
    <input class="form-control my-colorpicker1 colorpicker-element" type="text">
    <!-- Color Picker -->
</div>
<!-- /.form-group -->


<div class="bootstrap-timepicker">
	<div class="form-group">
		  <label>Time picker:</label>

		  <div class="input-group">
		    <input class="form-control timepicker" type="text">

		    <div class="input-group-addon">
		      <i class="fa fa-clock-o"></i>
		    </div>
		  </div>
		  <!-- /.Time Picker -->
	</div>
</div> 
<!-- /.form-group -->


<div class="form-group">
    <label>Intl US phone mask:</label>

    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-phone"></i>
      </div>
      <input class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask="" type="text">
    </div>
    <!-- /.input group -->
</div>
<!-- /.form-group -->

</div>

<!-- Advanced Editor -->


</div>
<!-- /.col -->  










@endsection



@push('script')

<!-- Add Script -->
<!-- Select2 -->
<script src="{{ asset('back-end/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('back-end/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('back-end/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('back-end/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('back-end/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap color picker -->
<script src="{{ asset('back-end/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ asset('back-end/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('back-end/plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<!-- Add Script END -->

@endpush




@push('script-inline')
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
@endpush

