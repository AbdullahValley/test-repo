@extends('admin.layouts.master')

@section('title-tag','View Single Course Details')

@section('box-title')
Single Course Information
@endsection

@section('content')

<!-- /.box-header -->
<div class="box-body">





    <div class="col-md-8 col-md-offset-2">

        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>mobile</th>
                    <th colspan="3" style="text-align: center !important">Action</th>
                </tr>
            </thead>
            <tbody>


            <tr>
                <td> John </td>
                <td>John@gmail.com</td>
                <td>01715 000 000</td>

                <td style="padding:0 15px;" class="text-center">
                    <a href="{{url('/admin/student/show/1')}}" class="btn btn-primary">View</a>
                </td>
                <td style="padding:0 15px;" class="text-center">
                    <a href="{{url('/admin/student/edit')}}" class="btn btn-primary">Edit</a>
                </td>
                <td style="padding:0 15px;" class="text-center">
                    <a href="{{url('/admin/student/delete')}}" class="btn btn-primary">Delete</a>
                </td>

            </tr>
            </tbody>
        </table>
    </div>

</div>

@endsection

