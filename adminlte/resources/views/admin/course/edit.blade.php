@extends('admin.layouts.master')

@section('title-tag','Update Course')

@section('box-title')
Update Course
@endsection

@section('content')

<!-- /.box-header -->
<div class="box-body">


    <div class="col-md-6 col-md-offset-3">
        <!-- general form elements -->
        <div class="box box-primary">


            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('admin/student') }}" method="POST">

                {{ csrf_field() }}

                <div class="box-body">

                    <div class="form-group">
                        <label for="name">Course Name</label>
                        <input class="form-control"  name="name" id="name" placeholder="Enter Course Name" type="text">
                    </div>
                    <div class="form-group">
                        <label for="code">Course Code</label>
                        <input class="form-control" name="code" id="code" placeholder="Enter Course Code" type="text">
                    </div>
                    <div class="form-group">
                        <label for="details">Details</label>
                        <input class="form-control" name="details" id="details" placeholder="Enter Course Details" type="tel">
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
        <!-- /.box -->

    </div>


</div>

@endsection

