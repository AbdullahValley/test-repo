<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('admin/', function () {

    return view('admin.index');

});





Route::prefix('admin/student')->group(function() {


    Route::get('/', 'StudentsController@index');
    // All Records Page


    Route::get('/create', 'StudentsController@create');
    // Create Page

    Route::post('/', 'StudentsController@store');
    // Submit Form -> Store Page


    Route::get('/edit/{id}', 'StudentsController@edit');
    // Update Record
    //    return view('admin.student.update', compact('id'));


    Route::get('/show/{id}', 'StudentsController@show');
    // Single View
    // return view('admin.student.single-view', compact('id'));


    Route::get('/delete/{id}', 'StudentsController@delete');
    // Delete Record
    // return view('admin.student.delete', compact('id'));


    Route::get('/trash', 'StudentsController@trash');
    // Trash
    // return view('admin.student.trash');




});




Route::prefix('admin/course')->group(function() {


    Route::get('/', 'CoursesController@index');
    // All Records Page


    Route::get('/create', 'CoursesController@create');
    // Create Page

    Route::post('/', 'CoursesController@store');
    // Submit Form -> Store Page


    Route::get('/edit/{id}', 'CoursesController@edit');
    // Update Record
    //    return view('admin.student.update', compact('id'));


    Route::get('/show/{id}', 'CoursesController@show');
    // Single View
    // return view('admin.student.single-view', compact('id'));


    Route::get('/delete/{id}', 'CoursesController@delete');
    // Delete Record
    // return view('admin.student.delete', compact('id'));


    Route::get('/trash', 'CoursesController@trash');
    // Trash
    // return view('admin.student.trash');

});



// Abdullah Al Noman


Route::prefix('admin/assign-course')->group(function() {


    Route::get('/', 'AssignCourseController@create');

    Route::post('/', 'AssignCourseController@store');

    // Route::get('/edit/', 'AssignCourseController@edit');

    //    return view('admin.student.update', compact('id'));

});




Route::prefix('admin/course-student')->group(function() {


    Route::get('/', 'CourseStudentController@index');
    // All Records Page



    Route::get('/show/{id}', 'CourseStudentController@show');







});