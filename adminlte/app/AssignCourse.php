<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignCourse extends Model
{
    protected $fillable = ['id','course_id','student_id'];
}
