<?php

namespace App\Http\Controllers;

use App\AssignCourse;
use App\Course;
use App\Student;
use Illuminate\Http\Request;

class AssignCourseController extends Controller
{
    public function create()
    {
        $students = Student::all();
        $courses = Course::all();
        return view('admin.assign-course.assign', compact('students','courses'));
    }


    public function store(Request $request)
    {

        //dd($request);
        //dd($data['course_id']);
        //AssignCourse::create($request->all());

        //return redirect('admin/assign-course');

        $data = $request->all();
        $token = $data['_token'];
        $course_list = $data['course_id'];
        $student = $data['student_id'];

        foreach ($course_list as $course){
            AssignCourse::create(['_token'=>$token,'student_id'=>$student,'course_id'=>$course]);
        }

    }


    public function edit()
    {
        //return view('admin.assign-course.edit');

//        Update student Courses
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }


    public function delete($id)
    {
        return view('admin.assign-course.delete', compact('id'));
    }

    public function trash()
    {
        return view('admin.assign-course.trash');
    }

}
